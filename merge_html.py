from requests_html import HTML
import pandas as pd
from constants import FIELDS_WITH_XPATHS
from pathlib import Path

def merge_html():

    input_path = Path('output')
    all_html_files = input_path.glob('*.html')
    all_dfs = []

    for file in all_html_files:
        print(file.name)
        defendant_df = pd.DataFrame(columns=FIELDS_WITH_XPATHS.keys())

        with open(file) as infile:

            source_code = infile.read()
            case_html = HTML(html=source_code)
            case_sections = case_html.find(".crim-history-row")

            for i in range(len(case_sections)):
                row = []
                for xpath in FIELDS_WITH_XPATHS.values():
                    element = case_sections[i].xpath(xpath, first=True)
                    value = ''
                    if element:
                        value = element.text
                    row.append(value)
                defendant_df.loc[i] = row

        all_dfs.append(defendant_df)

    merged = pd.concat(all_dfs).drop_duplicates()

    with open('all_results.csv', 'w+') as outfile:
        merged.to_csv(outfile, index=False)


if __name__ == "__main__":
    merge_html()

