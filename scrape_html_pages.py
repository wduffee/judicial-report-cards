from datetime import datetime, timedelta
from requests_html import HTMLSession, HTML
from input_dates import input_dates

""" Defines a method that will request the Detailed Criminal History pages
    of all defendants appearing before the Criminal Court within a chosen timeframe and save those
    html documents to the /output directory 
"""


def scrape(starting_date: datetime, ending_date: datetime):
    base_url = 'https://sci.ccc.nashville.gov'
    session = HTMLSession()

    # Scrape_range is a timedelta instance with a 'days' attribute used as the range incrementer
    scrape_range = starting_date - ending_date

    current_scrape_date = starting_date

    for day in range(scrape_range.days):
        print(f'scraping {current_scrape_date}')

        scrape_date_string = current_scrape_date.strftime('%m/%d/%Y')
        search_page_response = session.post(f'{base_url}/Reporting/TrialCourtScheduledAppearance',
                                            data={f'reportDate': {scrape_date_string}})

        defendant_links = search_page_response.html.find("[data-content='View Defendant Details']")

        identified_defendants = []

        for link in defendant_links:
            url_sections = link.attrs['href'].split('/')
            name_identifier = url_sections[3]
            if name_identifier not in identified_defendants:
                defendant_details_page_url = f'{base_url}/Search/CriminalHistory?P_CASE_IDENTIFIER={name_identifier}'
                name_page_response = session.get(defendant_details_page_url)
                name_page_html = HTML(html=name_page_response.text)
                with open(f"output/{name_identifier}.html", "w") as outfile:
                    outfile.write(name_page_html.html)
                identified_defendants.append(name_identifier)

        current_scrape_date = current_scrape_date - timedelta(days=1)


# Execution
if __name__ == '__main__':
    start, end = input_dates()
    scrape(start, end)
