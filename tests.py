import unittest

from datetime import datetime, timedelta
from unittest.mock import Mock, patch

import pytest

from input_dates import input_dates


class TestInputMethods(unittest.TestCase):

    def setUp(self) -> None:
        self.today = datetime.today()

    @patch('input_dates.get_todays_date')
    @patch('input_dates.get_end_date')
    @patch('input_dates.get_start_date')
    def test_default_values(self, start_date_mock: Mock,
                            end_date_mock: Mock, todays_date_mock: Mock):
        start_date_mock.return_value = ''
        end_date_mock.return_value = ''
        today = datetime.today()
        todays_date_mock.return_value = today
        start, end = input_dates()
        self.assertEqual((start, end), (today, today - timedelta(days=1)))

    @patch('builtins.print')
    @patch('input_dates.get_end_date')
    @patch('input_dates.get_start_date')
    def test_invalid_start_value(self, start_date_mock: Mock, end_date_mock: Mock,
                                 print_mock: Mock):
        start_date_mock.side_effect = ['IM A VERY BAD DATE', '']
        end_date_mock.return_value = ''
        input_dates()
        print_mock.assert_any_call("Invalid start date")

    @patch('builtins.print')
    @patch('input_dates.get_end_date')
    @patch('input_dates.get_start_date')
    def test_invalid_end_date_value_response(self, start_date_mock: Mock, end_date_mock: Mock,
                                 print_mock: Mock):
        start_date_mock.return_value = ''
        end_date_mock.side_effect = ['balderdash i say', '']
        input_dates()
        print_mock.assert_any_call("Invalid end date format")

    @patch('builtins.print')
    @patch('input_dates.get_end_date')
    @patch('input_dates.get_todays_date')
    @patch('input_dates.get_start_date')
    def test_end_date_out_range_response(self, start_date_mock: Mock, todays_date_mock: Mock,
                                         end_date_mock: Mock, print_mock: Mock):
        start_date_mock.return_value = ''
        todays_date_mock.return_value = self.today
        future_date_string = (self.today + timedelta(days=1)).strftime('%m%d%Y')
        end_date_mock.side_effect = [future_date_string, '']

        input_dates()
        print_mock.assert_any_call("Invalid time range (ending date must be earlier than start date)")


if __name__ == '__main__':
    unittest.main()
