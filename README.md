# judicial-reportcards

Repository for the judicial scorecard project with the Nashville NAACP

Dependencies:
* Python 3.9
* Pipenv

Quick start:
1) Run `pipenv install` to create a virtual environment and install all project-specific dependencies
2) Run `pipenv run python reportcards.py`
